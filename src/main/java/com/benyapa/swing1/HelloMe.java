/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener {
    
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener : Action");
    }
    
}

/**
 *
 * @author bwstx
 */
public class HelloMe implements ActionListener {
    
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me!"); //ชื่อขอบบน
        frmMain.setSize(500, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmMain.getContentPane().setBackground(new Color(240, 217, 255)); //พื้นหลังเฟรม

        //หัวข้อความ
        JLabel labelYourName = new JLabel("Your name :");
        labelYourName.setSize(80, 20);
        labelYourName.setLocation(10, 10);
        labelYourName.setBackground(Color.WHITE);
        labelYourName.setOpaque(true);
        
        JTextField txtName = new JTextField();//กรอบข้อความ
        txtName.setSize(200, 20);
        txtName.setLocation(95, 10); //กรอบข้อความ (แนวนอน,แนวตั้ง)
        txtName.setBackground(new Color(243, 241, 245));
        
        JButton btnHello = new JButton("Hello");
        btnHello.setSize(100, 20);
        btnHello.setLocation(95, 35);
        btnHello.setBackground(Color.LIGHT_GRAY);
        
        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);
        btnHello.addActionListener(new HelloMe());
        
        ActionListener actionListener = new ActionListener() { //Anonymous Class
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class : Action");
            }
            
        };
        btnHello.addActionListener(actionListener);
        
        JLabel labelHello = new JLabel("Hello..", JLabel.CENTER);
        labelHello.setSize(200, 20);
        labelHello.setLocation(95, 60);
        labelHello.setBackground(Color.WHITE);
        labelHello.setOpaque(true);//แสดงพื้นหลังข้อความสีทึบ

        frmMain.setLayout(null);
        frmMain.setVisible(true);
        frmMain.add(labelYourName);
        frmMain.add(txtName);
        frmMain.add(btnHello);
        frmMain.add(labelHello);

        //ดึงข้อมูลจากtxt มาแสดงผลโดยการคลิก
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtName.getText();
                labelHello.setText("Hello.." + name + " !!");
            }
            
        });
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloMe : Action");
    }
}
